---
title: "Route modelling exercise"
author: "Toby Wilkinson"
date: "23 Apr 2024"
---

# Route modelling with cost/friction functions: toy learning exercise

*Task*: build a model of "cost" or "friction" using a DTM
(digital terrain model) in order to reconstruct the possible
routes of the "Sacred Way" between Miletus and Didyma in Asia
Minor (modern Turkey).

*Learning objectives*: understand how a "cost model" is constructed
and how important this process for the efficacy of this type of analysis.

*Further reflection*: what does comparing different models of the same case
allow us to do?


## The archaeological case-study: a simple introduction to the Milesian Sacred Way

The ancient city of Miletus was connected to the nearby oracular
sanctuary of Apollo at Didyma by a road. Textual sources mention a
"sacred road", along which religious and civic processions were
undertaken at certain calendrical dates, and along the way there
appear to have been certain "stations", where rituals were performed.

In the early 20th century, a topographic survey was undertaken of
the "Milesian peninsula" by Paul Wilski (working as part of the
German expedition at Miletus). He identified what he called an
_Antike Strasse_ (an ancient street), passing through the hills,
cut into the rock and with flanking curb-stones.

![](images/01_wilski_map.jpg)

Later generations of archaeologists identified a paved street to
the north-west of the temple at Didyma, along which statues had
previously been lined before they were exported in the early 1800s.
(Some of these are now in the British Museum in London).

![](images/02_paved_street.jpg)

While this "Antike Strasse" and the paved road at Didyma are well
documented, the route of the intervening road is unclear - despite
excavations designed to locate it. Was it never paved the whole route?
Could it have changed route over the course of its 600-800 year
period of usage? Was there only one route? There are many questions here.

Let's use some "cost" modelling to start to think about movement on the
peninsula.


## A reminder: some caveats about least-cost

We're going to use "least-cost" analysis to reconstruct
hypothetical routes across a landscape. A *least-cost-path* (LCP) is a single
spatial line from one point to another which is calculated as generating the
least time or energy (depending on how you measure "cost" or "friction") for
an agent (e.g. a walker) to travel along it. As a single path, it is always
wrong, although it can be useful to think with.

Here instead we're going to make least-cost *corridors* (LCC). They are similar,
but they have the advantage of providing a "range" of movement possibilities,
rather than a single line. To do so we create 2 cumulative cost rasters and
then average them.

We should remember that both LCPs and LCCs assume that the agent wants to travel
at least "cost" (time/energy). In practice, single agents do not always want to
do that. They may have other motivations which are not dictated by rational
efficiency. However, establishing a base-line of rational efficiency
and identifying real world divergences from it, can help us to
hypothesize and test what those alternative motivations could be.

(Note that "cost" or "friction" may be used interchangeably in this tutorial, as
they represent different ways of thinking about the same computational
approach.)


## Setting up your system

There are many different ways to create cost distances,
least-cost-paths, cost corridors or circuitscapes. You can use tools
such as QGIS, ArcGIS, GRASS or code in Python, or R. For the sake of
introductory simplicity, we're going to use a plugin in QGIS.

If you don't already have it, download QGIS and install from here:-
- https://www.qgis.org/

## 1. Designing a model of cost

The first step in any cost-based route modelling is to create a base
*cost model*. There are at least 3 things we need to think about before creating
a model (1) what is the "agent" that is going to travel, (2) what things
represent cost for or friction against that agent travelling, and (3) how do
we explain these costs numerically?

1. We'll assume here that our agent is a pedestrian human -- and this is typical
in most archaeological approaches using this method (although we should remember
that humans also use other forms of transport, even in the past, and the way
we create the model may be different -- or we may want to compare multiple
different modes of travel using different models).

2. The biggest and most obvious cost to a pedestrian is, naturally, the energy or time cost of walking up and down slopes. For this reason, most archaeological
approaches tend to start with topography (in the form of a DTM or digital
terrain model). There are obvious many other factors we might (and perhaps
should) consider, and their relative importance may depend on scale. For
example, water availability or accessibility is likely to be a very important
factor in arid environments, since pedestrians will not travel far in a
desert. We'll ignore these factors in this example, since we are modelling a
short distance (<15km) in an area which is relatively well watered.

3. Finally, the DTM provides a map of topography measured in elevation, but it
is not raw elevation which is the "cost" to the pedestrian, it is the effect of
changing elevation, i.e. slope, on the speed that the walker can travel, or the
amount of energy they need to do so. So we will need to convert the values of
elevation into a measure of cost. In this case, we'll convert elevation to
time, and explain how we've done that.


## 2. Making the models

Open QGIS and load up the raster DTM from `/dtm_AD1910/dtm_wilksi_AD1910_small.tif`
It should look something like this:-

![](images/03_dtm.jpg)

The elevations in this raster need to be converted into a time cost for a
human pedestrian. How do we do this?

There is a lot of literature about alternative ways to calculate this (take a
look at the publications of Irmela Herzog for some background). For a
pedestrian walking up and down slopes, we can say that the cost is
"anisotropic" (i.e. it depends on the direction), because walking up a slope
of a certain angle of slope does not require the same amount of time or energy
as walking down at the same angle of slope. We need what is called a
"Hiking Function".

As this is a toy example we'll start just one common formula, which converts
slope into time, known as Tobler's Hiking Function (see [Wikipedia entry for
background info](https://en.wikipedia.org/wiki/Tobler%27s_hiking_function) ).

![](images/04_tobler.svg)

The default function converts slope to speed, but cost is the inverse of speed
so what we want is "pace". Tobler's function (originally based on empirical
data), suggests we can calculate pace (how many seconds per metre) using the
following formula, where m = gradient (dh/dx), which can be also calculated
as tan θ (where θ = angle of slope):-

$$p=0.6e^3.5m+0.05$$

![](images/05_tobler_pace.png)

This way, the resulting "cost" model will be measured in time (per metre).

**IMPORTANT NOTE: Note that Tobler's formula is anisotropic - it takes account
of up/down slopes but for simplicity in this toy exercise we are only going to
use it isotropically (i.e. we will ignore direction of slope).**


### 2.a Find gradient/slope

First then let's create a slope raster from the elevation. You'll find a
convenient function to do this in the Processing Toolbox > GDAL > Slope. The
dialogue box looks like this:-

![](images/06_gdal_slope.jpg)

Once generated you'll be a black and white raster on top. It'll look like this:-

![](images/07_slope_result1.jpg)

Make it a bit easier to read by changing the symbology to "Singleband pseudocolor"

![](images/08_slope_rsymbology.jpg)

You'll get something more understandable like this:-

![](images/09_slope_result2.jpg)


### 2.b Convert gradient to pace

To use Tobler's formula, we need to use a Raster Calculator. QGIS has
different calculators, but for us the best one is GRASS's one which
you'll find again in the Processing Toolbox:-

(Top tip:- you can just search for `mapcalc.simple` in the search box at the
top of the processing toolbox.)

![](images/10_raster_calculator.jpg)

The Tobler formula needs to be converted into a format that QGIS can understand.

e is Euler's number and approximately= 2.71828. So assuming the slope raster
you generated is called "Slope", then the Formula to enter is the following.
Make sure the slope raster is selected as "raster A!"

<!--```
0.6*2.71828 ^ (3.5*(abs(tan("Slope@1") + 0.05)))
```
-->
```
0.6*(2.71828 ^ (3.5*(abs(tan(A) + 0.05))))
```

Once you've run the analysis, you'll want to change the Symbology of the
resulting "Calculated" layer to Singleband pseudocolor again to make it
understandable visually:-

![](images/11_cost_model.jpg)

There's an additional step which you may have to do, because GRASS/QGIS
produces an "indexed" raster instead of a numeric one. The easiest thing to
do is the "Export" the raster (right click on the Calculated layer) as a TIF
with a name like "costmodel1.tif" in a `models` directory:

![](images/12_export_as_tif.jpg)

![](images/13_export_as_tif2.jpg)


### 2.c Create a cumulative cost distance raster

First we should open up some vector layers so we know our start and end points.
You'll find 3 layers you'll want to load in the `places_of_interest` folder:

- `didyma_temple_of_apollo.geojson` - the location of the temple
- `wilski_antikestrasse.geojson` - a line showing the line of the Antike Strasse
- `road_as_leaves_hills.geojson` - the place where Wilski's Antike Strasse leaves
the hills

(An aside: note these GeoJSON files are in UTM35N projection, which is not
standard for web GeoJSON, but it's the most compact format for transmitting
these sort of shapes.)

In QGIS, open them via Layer > Add Layer > Add Vector layer... box or the
Data Manager.

![](images/14_open_vector.jpg)

Now open the GRASS `r.cost` cost distance generator tool from the processing
toolbox. Choose your cost model as the "unit cost layer", and then the
`road_as_leaves_hills` as "start points [optional]".

![](images/15_rcost_box.jpg)

(Lower down in this dialogue, you can untick under "Cost allocation map" and
"Movement directions" for now -- we don't need them, but you can play them
later if you want to!). If you like, save the result as `cumcost1.tif`

![](images/16_rcost_box2.jpg)

You should get something like this (after you've changed the symbology!)

![](images/17_rcost_result1.jpg)

<!--
Now open the Least Cost Path tool from the Processing Toolbox, and fill out
the values: Cost raster layer, Cost raster band (Band 1), the Start-point (
road_as_leaves_hills), End point (didyma_temple_of_apollo):-

![](images/15_cd_tool.jpg)

Let's run it and see what we get. It'll be something like the following (you
may want to change the Symbology again to make the line more visible):-

![](images/16_lcp_result.jpg)

You can see that the line is not the straightest route from point to point, but
one which snakes around the slopes.

You can export the line of the least-cost path as a GeoJSON file to save it for
later.
-->

### 2.d Create a cumulative cost distance raster for the second points

Now do the same again, but this time use the other point (`didyma_temple_of_apollo`)
as the start point. You'll end up with:-

![](images/18_rcost_result2.jpg)

### 2.e Average the two to make a corridors

Let's use the `r.mapcalc.simple` raster calculator again, with a simpler formula,
and setting A and B to our 2 cumulative cost distance rasters (here `cumcost1`
and `cumcost2`):

```
(A+B)/2
```

![](images/19_calc_corridor.jpg)

The result is an average. You'll then need to play with the Symbology of the
layer to make it into something meaningful. Set the "maximum" (e.g. to 700) and
make sure you "clip out of range values"...

![](images/20_corridor_symbology.jpg)


## 3. Reflecting on the results

The result will be something like the following, which shows a probability
corridor of the easiest routes between the two points, according to our
hiking model, and the areas which the topography would predict the most
common natural paths to be created:

![](images/21_corridor_result.jpg)

We can then think about how many different paths a road could take between the
hills and the temple at Didyma... It's possible the road could have changed
route through time, even if "least cost" was the main motivating factor.

Of course we then need some on-the-ground archaeology to confirm or reject...!
Computer models are never the end of the discussion.


## **Independent exercise/homework** (obligatory)

Now have a go at creating your own model of cost using the procedures outlined above
and this case-study or else find a case-study and dataset of your own.

How does the route of a least-cost-path change when you change the formula
of the model to some example values below:-

- `0.6*(2.71828 ^ (50*(abs(tan(A) + 0.05))))`
- `0.1*(2.71828 ^ (3.5*(abs(tan(A) + 0.05))))`

(Note these do not provide realistic "cost" values on time, but they do help
to see what effect the different parts of the formula have: this is called
sensitivity analysis, and is important for testing the limitations of our
models).

Save the resulting least-cost corridors in a screenshot and write a
brief report on how different the results are (or not!).

--------------

## **Extra Exercise 1: Isochrones** (optional)!

Using the cumulative cost distance created in Step 2d above, you could use a
contour tool (for example, in QGIS, Raster > Extraction > Contour) to create
isochrone contours -- i.e. lines which show every 5 minutes walking time
away from the temple of Didyma.

This is useful when we think about the relationship between settlements and
their resources in a landscape.

![](images/22_isochrones.jpg)


## **Extra Exercise 2: Landscape change** (optional)!

This is only if you are having fun with cost modelling...!

Use the same models with a different DTM, for example a hypothetical
reconstructed coastline from 800BC instead of the modern one (you'll find an
example as `dtm_800BC/dtm_hypothetical_800BC_small.tif` ).

How would comparing resulting least-cost corridors with different models help us
to understand the history of the road?

--------------

## Taking route modelling forward...

There are many alternative tools which are more sophisticated (such as
`r.walk`) which take account of direction and elevation in better ways. Also  
least cost paths are problematic in themselves, in that they are single paths
and so must "always" be wrong at some level. Better ways to think about
routes is with corridors or pinch-points (via electricity conductivity theory).

If you want to take this approach forward, you should look into these
more sophisticated approaches and always ask:- what are the limitations of my
model and my approach?...

--------------

Have a look here for some more info about the Sacred Way case-study:-
- https://www.projectpanormos.com/sacredway/intro/
